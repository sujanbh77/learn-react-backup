import React, { useState } from "react";
import LearnCleanUpFunction2 from "./ComponentPractice/LearnUseEffect/LearnCleanUpFunction2";
import Form1 from "./ComponentPractice/LearnForm/Form1";
import Form2 from "./ComponentPractice/LearnForm/Form2";
import PersonForm from "./ComponentPractice/LearnForm/HW/PersonForm";
import ProductForm from "./ComponentPractice/LearnForm/HW/ProductForm";
import FoodForm from "./ComponentPractice/LearnForm/HW/FoodForm";
import Form3 from "./ComponentPractice/LearnForm/Form3";
import SelectHW from "./ComponentPractice/LearnForm/HW/SelectHW";
import LearnOrNullish from "./ComponentPractice/LearnOrNullish";
import Form4 from "./ComponentPractice/LearnForm/Form4";
import Form5 from "./ComponentPractice/LearnForm/Form5";
import Form6 from "./ComponentPractice/LearnForm/Form6";
import LearnUseRef from "./ComponentPractice/LearnUseRef/LearnUseRef";
import FormikForm from "./ComponentPractice/LearnFormik/FormikForm";
import CreateProduct from "./ComponentPractice/LearnToHitApi/Product/CreateProduct";
import ReadProduct from "./ComponentPractice/LearnToHitApi/Product/ReadProduct";
import LearnLocalStorage from "./ComponentPractice/LearnLocalStorage/LearnLocalStorage";
import LearnRoute from "./ComponentPractice/LearnRouting/LearnRoute";

const App = () => {
  // let [show1, setShow1] = useState(true);

  return (
    <div>
      {/* <LearntoHandleArray></LearntoHandleArray> */}
      {/* {show1 ? <LearnCleanUpFunction2></LearnCleanUpFunction2> : null}
      <button
        onClick={() => {
          setShow1(true);
        }}
      >
        Show
      </button>
      <button
        onClick={() => {
          setShow1(false);
        }}
      >
        Hide
      </button> */}
      {/* <Form1></Form1> */}
      {/* <Form2></Form2> */}
      {/* <PersonForm></PersonForm>
      <br></br>
      <br></br>
      <ProductForm></ProductForm>
      <br></br>
      <br></br>
      <FoodForm></FoodForm> */}
      {/* <Form3></Form3> */}
      {/* <SelectHW></SelectHW> */}
      {/* <LearnOrNullish></LearnOrNullish> */}
      {/* <Form4></Form4> */}
      {/* <Form5></Form5> */}
      {/* <Form6></Form6> */}
      {/* <LearnUseRef></LearnUseRef> */}
      {/* <FormikForm></FormikForm> */}
      {/* <CreateProduct></CreateProduct> */}
      {/* <ReadProduct></ReadProduct> */}
      {/* <LearnLocalStorage></LearnLocalStorage> */}
      {/* <LearnRoute></LearnRoute> */}
      
    </div>
  );
};

export default App;
