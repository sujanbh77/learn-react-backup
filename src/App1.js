// import React from "react";

// const App = (props) => {
//   // let { name, address, age } = props;
//   // console.log("all data", props);
//   // console.log("name", props.name);
//   // console.log("address", props.address);
//   // console.log("isMarried", props.isMarried);
//   // console.log("age", props.age);

//   // Ternary Operator:

//   // let n1 = "nitan"
//   // let check = n1 === "nitan" ? "A"
//   //            :n1 === "prashant" ? "B"
//   //            :"C"
//   //            console.log(check)

//   // ...........................................

//   // let isMarried = true
//   // let message = isMarried === true ? "He is married." : "He is not married."
//   // console.log(message)

//   // ..............................................

//   // let age1 = 19
//   // let message = age1 > 17 ? "You can enter the room."
//   //                          :"You cannot enter the room."
//   // console.log(message)

//   // ..............................................

//   // let age = 19;
//   // let check =
//   //   age === 16
//   //     ? "Your age is 16."
//   //     : age === 17
//   //     ? "Your age is 17."
//   //     : age === 18
//   //     ? "Your age is 18."
//   //     : "Your age is neither 16, 17, or 18.";
//   // console.log(check)

//   // ..............................................

//   let add1 = 1
//   let add2 = <div>Hello World</div>

//   return (
//     <div>
//       {/* Props: */}

//       {/* <div style = {{backgroundColor:"red", color:"white"}}>Hello</div>
//       <div>{1 + 2}</div>
//       <div>name is {name}</div>
//       <div>address is {address}</div>
//       <div>age is {age}</div> */}

//       {/* --------------------------------- */}

//       {/* onClick event: */}

//       {/* <div
//       onClick = {()=>{
//         console.log("I am div.")
//       }}>This is dev.</div> */}
//       {/* <button
//       onClick = {() => {
//         console.log("My name is Sujan.")
//       }}
//       >Click Me</button> */}

//       {/* ---------------------------- */}

//       {/* Calling variable storing tags in html. */}

//       {add1}
//       {add2}

//     </div>
//   );
// };

// export default App;
