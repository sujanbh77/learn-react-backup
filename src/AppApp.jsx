import React from "react";
import { NavLink, Route, Routes } from "react-router-dom";
import CreateProduct from "./ComponentPractice/LearnToHitApi/Product/CreateProduct";
import ReadProduct from "./ComponentPractice/LearnToHitApi/Product/ReadProduct";
// import UpdateForm from "./ComponentPractice/LearnForm/HW/UpdateProductForm";
import EditForm from "./ComponentPractice/LearnToHitApi/Product/UpdateProduct";

const AppApp = () => {
  return (
    <div>
      <NavLink to="/products/create" style={{ marginLeft: "20px" }}>
        create
      </NavLink>
      <NavLink to="/products" style={{ marginLeft: "20px" }}>
        products
      </NavLink>

      <Routes>
        <Route
          path="/products/create"
          element={
            <div>
              <CreateProduct></CreateProduct>
            </div>
          }
        ></Route>
        <Route
          path="/products"
          element={
            <div>
              <ReadProduct></ReadProduct>
            </div>
          }
        ></Route>

        <Route
          path="/products/edit/:id"
          element={
            <div>
              <EditForm></EditForm>
            </div>
          }
        ></Route>
      </Routes>
    </div>
  );
};

export default AppApp;
