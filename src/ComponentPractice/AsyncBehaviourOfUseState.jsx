import React, { useState } from 'react'

const AsyncBehaviourOfUseState = () => {
    let [name, setName] = useState("nitan1")
    console.log(name)
  return (
    <div>
      name is {name}
      <br></br>
      <button onClick = {() => {
        setName("nitan2")
        setName("nitan3")
        setName("nitan4")
      }}>Change Name</button>
    </div>
  )
}

export default AsyncBehaviourOfUseState
