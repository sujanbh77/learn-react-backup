import React, { useState } from 'react'

const AsyncBehaviourOfUseState2 = () => {
    let [element, setElement] = useState(1)
    console.log(element)
  return (
    <div>
        element is {element}
      <button
      onClick = {()=> {
        [2, 3, 4].map((value, i) => {
            setElement(value)
        })
      }}
      >Click Me!</button>
    </div>
  )
}

export default AsyncBehaviourOfUseState2
