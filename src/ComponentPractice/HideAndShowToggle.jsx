// Hide/Show image using toggle button.

import React, { useState } from 'react'

const HideAndShowToggle = () => {
    let [showImg, setShowImg] = useState(true)
  return (
    <div>
      {showImg ? <img src = "./favicon.ico" alt = "favicon"></img> : null}
      <br></br>
      <button
      onClick = {()=>{
        setShowImg(!showImg)
      }}
      >Toggle</button>
    </div>
  )
}

export default HideAndShowToggle
