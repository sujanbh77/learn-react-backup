import React, { useState } from "react";

const Form3 = () => {
  let [name, setName] = useState("");
  let [description, setDescription] = useState("");
  let [country, setCountry] = useState("");

  let countryOptions = [
    { label: "Choose Country", value: ""},
    { label: "Nepal", value: "nepal" },
    { label: "Finland", value: "finland" },
    { label: "China", value: "china" },
    { label: "India", value: "india" },
  ];

  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          let info = {
            name: name,
            description: description,
            country: country,
          };
          console.log(info)
          console.log(info.name);
          console.log(info.description);
          console.log(info.country);
        }}
      >
        <div>
          <div>
            <label htmlFor="name">Name: </label>
            <input
              type="text"
              id="name"
              value={name}
              placeholder="Name"
              onChange={(e) => {
                setName(e.target.value);
              }}
            ></input>
          </div>
          <br></br>

          <div>
            <label htmlFor="description">Description: </label>
            <textarea
              id="description"
              value={description}
              placeholder="Write your description here."
              onChange={(e) => {
                setDescription(e.target.value);
              }}
            ></textarea>
          </div>
          <br></br>

          {/* <select
            value={country}
            onChange={(e) => {
              setCountry(e.target.value);
            }}
          >
            <option value="">Choose your country</option>
            <option value="nep">Nepal</option>
            <option value="fin">Finland</option>
            <option value="ind">India</option>
            <option value="chi">China</option>
          </select> */}
          
          <select
          value={country}
          onChange={(e)=>{
            setCountry(e.target.value)
          }}
          >
            {countryOptions.map((item, i) => {
                return <option key={i} value={item.value}>{item.label}</option>
            })}
          </select>
          <br></br>

          <div>
            <input type="checkbox"></input>I am submitting this form.
          </div>
          <br></br>
          <button type="submit">Submit</button>
        </div>
      </form>
    </div>
  );
};

export default Form3;
