import React, { useState } from "react";

const Form5 = () => {
  let [gender, setGender] = useState("");

  let handleSubmit = (e) => {
    e.preventDefault();
    let info = {
      gender: gender,
    };
    console.log(info.gender);
  };

  let genderOptions = [
    {label: "Male", value:"male"},
    {label: "Female", value:"female"},
    {label: "Other", value:"other"},
  ]

  return (
    <div>
      <form onSubmit={handleSubmit}>

        {/* <input
          type="radio"
          id="male"
          value="male"
          checked={gender === "male"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
        ></input>
        <label htmlFor="male">Male</label>
        <br></br>

        <input
          type="radio"
          id="female"
          value="female"
          checked={gender === "female"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
        ></input>
        <label htmlFor="female">Female</label>
        <br></br>

        <input
          type="radio"
          id="other"
          value="other"
          checked={gender === "other"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
        ></input>
        <label htmlFor="other">Other</label>
        <br></br> */}

        {genderOptions.map((item, i)=>{
            return (
                <>
                <input
                type="radio"
                id={item.value}
                value={item.value}
                checked={gender === item.value}
                onChange={(e) => {
                    setGender(e.target.value);
                 }}
                ></input>
                <label key = {i} htmlFor = {item.value}>{item.label}</label>
                </>
            )
        })}

        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default Form5;
