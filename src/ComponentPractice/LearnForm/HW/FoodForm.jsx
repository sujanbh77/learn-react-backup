import React, { useState } from 'react'

const FoodForm = () => {
    let [foodName, setFoodName] = useState("")
    let [foodPrice, setFoodPrice] = useState("")
    let [foodRating, setFoodRating] = useState("")
  return (
    <div>
        <form
        onSubmit={(e)=>{
            e.preventDefault()
            console.log("Form submitted successfully.")
        }}
        >

            <div>
                <label htmlFor="foodName">Food Name: </label>
                <input
                type="text"
                id="foodName"
                value={foodName}
                placeholder="Food Name"
                onChange = {(e)=>{
                    setFoodName(e.target.value)
                }}
                ></input>
            </div>
            <br></br>

            <div>
                <label htmlFor="foodPrice">Food Price: </label>
                <input
                type="number"
                id="foodPrice"
                value={foodPrice}
                placeholder="Food Price"
                onChange = {(e)=>{
                    setFoodPrice(e.target.value)
                }}
                ></input>
            </div>
            <br></br>

            <div>
                <label htmlFor="foodRating">Food Rating: </label>
                <input
                type="number"
                id="foodRating"
                value={foodRating}
                placeholder="Food Rating"
                onChange = {(e)=>{
                    setFoodRating(e.target.value)
                }}
                ></input>
            </div>
            <br></br>

            <div>
                <input type="checkbox"></input>
                I am submitting this form.
            </div>
            <br></br>
            <button type="submit">Submit</button>

        </form>
      
    </div>
  )
}

export default FoodForm
