import { Formik, Form, Field, yupToFormErrors } from "formik";
import React from "react";
import * as yup from "yup";

const FormikForm = () => {
  // Each field consists of three components(things):

  // Value
  // Error
  // Touch

  let initialValues = {
    firstName: "",
    lastName: "",
    description: "",
  };

  let onSubmit = (value, other) => {
    console.log(value);
  };

  let validationSchema = yup.object({
    firstName: yup.string().required("First Name is required"),
    lastName: yup.string().required("Last Name is required"),
    description: yup.string().required("Description is required"),
  });

  return (
    <div>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {(formik) => {
          // formik={
          //   setFieldValue
          // }
          return (
            <Form>
              <Field name="firstName">
                {({ field, form, meta }) => {
                  // meta = {
                  //   value: "...",
                  //   error: "...",
                  //   touched: false,
                  // }

                  return (
                    <div>
                      <label htmlFor="firstName">First Name: </label>
                      <input
                        type="text"
                        id="firstName"
                        value={meta.value}
                        placeholder="First Name"
                        onChange={(e) => {
                          // setFirstName(e.target.value)
                          formik.setFieldValue("firstName", e.target.value);
                        }}
                      ></input>
                      {meta.touched && meta.error ? (
                        <div style={{ color: "red" }}>{meta.error}</div>
                      ) : null}
                    </div>
                  );
                }}
              </Field>
              <br></br>
              <Field name="lastName">
                {({ field, form, meta }) => {
                  return (
                    <div>
                      <label htmlFor="lastName">Last Name: </label>
                      <input
                        type="text"
                        id="lastName"
                        value={meta.value}
                        placeholder="Last Name"
                        onChange={(e) => {
                          formik.setFieldValue("lastName", e.target.value);
                        }}
                      ></input>
                      {meta.touched && meta.error ? (
                        <div style={{ color: "red" }}>{meta.error}</div>
                      ) : null}
                    </div>
                  );
                }}
              </Field>
              <br></br>
              <Field name="description">
                {({ field, form, meta }) => {
                  return (
                    <div>
                      <label htmlFor="description">Description: </label>
                      <input
                        type="text"
                        id="description"
                        value={meta.value}
                        placeholder="Description"
                        onChange={(e) => {
                          formik.setFieldValue("description", e.target.value);
                        }}
                      ></input>
                      {meta.touched && meta.error ? (
                        <div style={{ color: "red" }}>{meta.error}</div>
                      ) : null}
                    </div>
                  );
                }}
              </Field>
              <br></br>

              <button type="submit">Submit</button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default FormikForm;
