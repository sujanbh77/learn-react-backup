import React from "react";
import { useSearchParams, useNavigate } from "react-router-dom"

// http://localhost:3000/about?name=Sujan&age=27&address=Samakhusi

const About = () => {
  const [searchParams] = useSearchParams();
  
  const navigate = useNavigate()

  return <div>
    About Page
    <br></br>
    Name: {searchParams.get("name")}
    <br></br>
    Age: {searchParams.get("age")}
    <br></br>
    Address: {searchParams.get("address")}
    <br></br>

    <button
    onClick={()=>{
      // navigate("/contact")
      navigate("/contact", {replace: true}) //Used for login. | To prevent multiple logins without logouts. | To prevent going to the previous page.
    }}
    >Go to Contact Page</button>
    </div>;
};

export default About; 
