import React from "react";
import { useParams } from "react-router-dom";

const GetDynamicRouteParameter = () => {
  // useParams() : To get dynamic route parameters
  // useSearchParams() : To get dynamic route parameters
  // useNavigate() : To get dynamic route parameters

  let params = useParams();

  return (
    <div>
      GetDynamicRouteParameter
      <br></br>
      {params.id1}
      <br></br>
      {params.id2}
    </div>
  );
};

export default GetDynamicRouteParameter;
