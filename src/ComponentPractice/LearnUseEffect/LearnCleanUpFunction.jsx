import React, { useEffect } from 'react'

// When component is unmounted; nothing is executed except clean up function.

// Mount:  Component mount.   : Show
// Unount: Component unmount. : Hide  

const LearnCleanUpFunction = () => {
  console.log("Hello, I will not execute when component is unmounted.")
  useEffect(()=>{
    console.log("I am useEffect, I will not execute when component is unmounted.")
    return () =>{
      console.log("I am clean up function, I will execute when the component is unmounted.")
    }
  }, [])

  return (
    <div>
      LearnCleanUpFunction
    </div>
  )
}

export default LearnCleanUpFunction
