import React, { useEffect, useState } from 'react'

// For Primitive:     Page renders when value changes.
// For Non-Primitive: Page renders when memory changes.

const LearnUseEffect2 = () => {
    let [ar1, setAr1] = useState([1, 2])

    useEffect(() => {
        console.log("I am useEffect.")
    }, [JSON.stringify(ar1)])
  return (
    <div>
      <button
      onClick = {()=>{
        setAr1([3, 4])
      }}
      >Click Me
      </button>
    </div>
  )
}

export default LearnUseEffect2
