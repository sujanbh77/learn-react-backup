import React, { useState } from 'react'

const LearntoHandleArray = () => {
    let [data, setData] = useState([
        {
            name: "nitan",
            age: 29,
            address: "gagalphedi",
            isMarried: false,
        },
        {
            name: "seer",
            age: 19,
            address: "sankhu",
            isMarried: false,

        },
        {
            name: "sujan",
            age: 27,
            isMarried: false,

        },
    ])
  return (
    <div>
      {data.map((value, i) => {
        return (
            <div key = {i} style={{ border: "solid black 3px", marginBottom: "10px" , background:"gray"}}>
                    <p>Name {value.name?value.name:"N/A"}</p>
                    <p>Age {value.age?value.age:"N/A"}</p>
                    <p>Address {value.address?value.address:"N/A"}</p>
                    <p>isMarried {value.isMarried?"Yes":"No"}</p>
            </div>
        )
      })}
    </div>
  )
}

export default LearntoHandleArray
