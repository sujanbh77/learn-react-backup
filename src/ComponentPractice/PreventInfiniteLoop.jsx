import React, { useState } from 'react'

const PreventInfiniteLoop = () => {
    let [count, setCount] = useState(0)
    // setCount(count + 1) //set... is asynchronous and is always executed last. (Not a promise|No reason for await)
  return (
    <div>
      count is {count}
      {/* To prevent infinite loop, always use set... at some event. ex: onClick */}
      <button
      onClick={()=>{
        setCount(count+1)
      }}
      >Increment</button>
    </div>
  )
}

export default PreventInfiniteLoop
