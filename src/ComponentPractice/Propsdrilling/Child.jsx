import React from 'react'

const Child = (props) => {
  let {name} = props
  return (
    <div>
      I am the Child. I call my Grandparent: {name}

    </div>
  )
}

export default Child
