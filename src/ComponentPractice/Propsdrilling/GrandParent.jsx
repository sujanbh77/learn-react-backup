import React, { useState } from 'react'
import Parent from './Parent'

const GrandParent = () => {
  let [name, setName] = useState("Hajurbau")
  return (
    <div>
      I am the Grand Parent.
      <Parent name = {name}></Parent>
    </div>
  )
}

export default GrandParent
